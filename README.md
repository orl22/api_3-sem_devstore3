# Sprint 3

<img src="/Imagens_Sprint3/logo.jpg"/>

## :computer: Proposta

Na Sprint 3,realizamos a montagem e planejamento de votação Planning Pokker,como tambem a criação do sistema de Chat para melhor comunicação dos usario durante a votação e salas para a realização para votação. Alem disso com um sistema de envio de E-mail atraves do Front-end para realizar convites para prticipação de atividades do projeto e por ultimo a tela de restrospectiva de controle e votação das atividades e sua respectiva listagem.

##  :chart_with_upwards_trend: Meta da Sprint
Para Sprint 3, estabelecemos como meta o aperfeiçoamento das telas do sistema de votação Planning Pokker com detalhes para melhor entendimento do cliente de como funcionara o sistema de votação no back end, com a inclusão do chat; descrição de atividades; níveis de prioridade e um sistema de criação de conta mais dinâmico com e-mail, confirmação de criação de contas e melhor layout de nossas telas para a facilitação do uso do sistema;


## :dart: Detalhes da Sprint:

**Implementações do back-end:**

- Criação de salas de votação no Planning Pokker.
- Votação do sistema Planning Pokker.
- inserção do sistema de chat para a melhor comunicação no sistema Planning Pokker.


**Implementações Front-End:**
-  tela de Restrospectiva inserida no projeto e listagem das atividades inseridas no sistema.
- inserção de sistema de envio de E-mail através do front end.



## Apresentação da Sprint
<a href="https://docs.google.com/presentation/d/1A_0bpcEt9JrdIQul4Jya0EcLO0azTlxl/edit#slide=id.p6">Apresentação</a>



## Wireframes:

- Em desenvolvimento



## :department_store: Proposta para a Proxima Sprint:
- Para a próxima sprint teremos o objetivo de desenvolver o sistema de resultado de votação realizado no em uma sala do sistema, quem participou da votação do sistema e inserção de itens para realizar sua avaliação e votação no sistema.

